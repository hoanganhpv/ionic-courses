const createAReviewMiddleware = (req, res, next) => {
    console.log(`Đang thực hiện xử lý reviewMiddleware tại tầng middleware để tạo mới 1 review cho course tương ứng...`);
    next();
};

const getAllReviewByIdMiddleware = (req, res, next) => {
    console.log(`Đang thực hiện xử lý reviewMiddleware tại tầng middleware để tìm ra danh sách review của course tương ứng...`);
    next();
};

const deleteAReviewByIdMiddleware = (req, res, next) => {
    console.log(`Đang thực hiện xử lý reviewMiddleware tại tầng middleware để xóa 1 review của course tương ứng...`);
    next();
};

const getAReviewsOfCourseMiddleware = (req, res, next) => {
    console.log(`Đang thực hiện xử lý reviewMiddleware tại tầng middleware để lấy ra 1 review của course tương ứng...`);
    next();
};

const updateAReviewOfCourseMiddleware = (req, res, next) => {
    console.log(`Đang thực hiện xử lý reviewMiddleware tại tầng middleware để cập nhật 1 review của course tương ứng...`);
    next();
};

export {
    createAReviewMiddleware,
    getAllReviewByIdMiddleware,
    deleteAReviewByIdMiddleware,
    getAReviewsOfCourseMiddleware,
    updateAReviewOfCourseMiddleware
}