const getAllCourseMiddleware = (req, res, next) => {
    console.log(`Đang thực hiện xử lý courseMiddleware tại tầng middle ware để lấy ra toàn bộ course ...`);
    next();
};

const getACourseByIdMiddleware = (req, res, next) => {
    console.log(`Đang thực hiện xử lý tại courseMiddleware tại tầng middleware để lấy ra 1 course theo id`);
    next();
};

const createACourseMiddleware = (req, res, next) => {
    console.log(`Đang thực hiện xử lý tại courseMiddleware tại tầng middleware để tạo ra 1 course theo id`);
    next();
};

const updateACourseByIdMiddleware = (req, res, next) => {
    console.log(`Đang thực hiện xử lý tại courseMiddleware tại tầng middleware để cập nhật 1 course theo id`);
    next();
};

const deleteACourseByIdMiddleware = (req, res, next) => {
    console.log(`Đang thực hiện xử lý tại courseMiddleware tại tầng middleware để xóa 1 course theo id`);
    next();
};

export {
    getAllCourseMiddleware,
    getACourseByIdMiddleware,
    createACourseMiddleware,
    updateACourseByIdMiddleware,
    deleteACourseByIdMiddleware
}