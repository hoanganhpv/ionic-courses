import express from "express";

import {
    getAllCourseMiddleware,
    getACourseByIdMiddleware,
    createACourseMiddleware,
    updateACourseByIdMiddleware,
    deleteACourseByIdMiddleware
} from '../middlewares/courseMiddleware.js';

import {
    getAllCourseController,
    getACourseByIdController,
    createACourseController,
    updateCourseByIdController,
    deleteACourseById
} from '../controllers/courseController.js'

const courseRouter = express.Router();

courseRouter.get('/courses', getAllCourseMiddleware, getAllCourseController);

courseRouter.get('/courses/:paramId', getACourseByIdMiddleware, getACourseByIdController);

courseRouter.post('/courses', createACourseMiddleware, createACourseController);

courseRouter.put('/courses/:paramId', updateACourseByIdMiddleware, updateCourseByIdController);

courseRouter.delete('/courses/:paramId', deleteACourseByIdMiddleware, deleteACourseById);


export { courseRouter };