import express from 'express';
import {
    createAReviewMiddleware,
    getAllReviewByIdMiddleware,
    deleteAReviewByIdMiddleware,
    getAReviewsOfCourseMiddleware,
    updateAReviewOfCourseMiddleware
} from '../middlewares/reviewMiddleware.js';

import {
    createAReviewController,
    getAllReviewOfCourseController,
    deleteAReviewOfCourse,
    getAReviewOfCourseController,
    updateAReviewOfCourseController
} from '../controllers/reviewController.js'

const reviewRouter = express.Router();

reviewRouter.post('/courses/:paramId/reviews', createAReviewMiddleware, createAReviewController);

reviewRouter.get('/courses/:paramId/reviews', getAllReviewByIdMiddleware, getAllReviewOfCourseController);

reviewRouter.get('/courses/:courseId/reviews/:reviewId', getAReviewsOfCourseMiddleware, getAReviewOfCourseController);

reviewRouter.put('/courses/:courseId/reviews/:reviewId', updateAReviewOfCourseMiddleware, updateAReviewOfCourseController);

reviewRouter.delete('/courses/:courseId/reviews/:reviewId', deleteAReviewByIdMiddleware, deleteAReviewOfCourse)

export {
    reviewRouter
};

