import mongoose from "mongoose";

const Schema = mongoose.Schema;

const courseSchema = Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    courseCode: {
        type: String,
        unique: true,
        require: true,
    },
    courseName: {
        type: String, 
        require: true,
    },
    price: {
        type: Number,
        require: true
    },
    discountPrice: {
        type: Number,
        require: true
    },
    duration: {
        type: String,
        require: true
    },
    level: {
        type: String,
        require: true
    },
    coverImage: {
        type: String,
        require: true
    },
    teacherName: {
        type: String,
        require: true
    },
    teacherPhoto: {
        type: String,
        require: true
    },
    isPopolar: {
        type: Boolean,
        default: true
    },
    isTrending: {
        type: Boolean,
        default: false
    },
    reviews: [{
        type: mongoose.Types.ObjectId,
        ref: "Review"
    }]
}, {
    timestamps: true
});

const Course = mongoose.model("Course", courseSchema);

export { Course };