import mongoose from "mongoose";

const Schema = mongoose.Schema;

const reviewSchema = Schema({
    stars: {
        type: Number,
        default: 0
    },
    note: {
        type: String,
        required: false
    }
}, {
    timestamps: true
});

const Review = mongoose.model('Review', reviewSchema);

export { Review };