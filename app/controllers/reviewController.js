import mongoose from "mongoose";
import { Course } from '../models/courseModel.js';
import { Review } from '../models/reviewModel.js';

const createAReviewController = async (req, res) => {
    try {
        const vCourseId = req.params.paramId;
        const { stars, note } = req.body;
        const vReviewData = new Review({
            _id: new mongoose.Types.ObjectId(),
            stars: stars,
            note: note
        });

        if (!mongoose.Types.ObjectId.isValid(vCourseId)) {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        // validate
        if (typeof (stars) !== 'number' || !note || typeof (note) !== 'string') {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        const vReviewCreateResult = await Review.create(vReviewData);

        const vAddReviewResult = await Course.findByIdAndUpdate(vCourseId, {
            $push: { reviews: vReviewCreateResult._id },
        });

        res.status(201).json({
            message: `Create new review successfully!`,
            result: vAddReviewResult
        });

    } catch (err) {
        console.log(err);
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
    };
};

const getAllReviewOfCourseController = async (req, res) => {
    try {
        const vCourseId = req.params.paramId;
        if (!mongoose.Types.ObjectId.isValid(vCourseId)) {
            return res.stars(400).json({
                errorMessage: `Bad request!`
            });
        };

        const vReviewGeted = await Course.findById(vCourseId).populate('reviews');
        res.status(200).json({
            message: {
                status: 'OK',
                result: vReviewGeted.reviews
            }
        });
    } catch (err) {
        console.log(err);
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
    };
};

const deleteAReviewOfCourse = async (req, res) => {
    try {
        const vCourseId = req.params.courseId;
        const vReviewId = req.params.reviewId;

        if (!mongoose.Types.ObjectId.isValid(vCourseId) || !mongoose.Types.ObjectId.isValid(vReviewId)) {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        const deleteReviewResult = await Review.findOneAndDelete({ _id: vReviewId });
        const deleteReviewIdInCourserResult = await Course.findByIdAndUpdate(vCourseId, {
            $pull: { reviews: vReviewId }
        });

        res.status(204).json({
            message: `delete OK!`
        });

    } catch (err) {
        console.log(err);
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
    };
};

const getAReviewOfCourseController = async (req, res) => {
    try {
        const vCourseId = req.params.courseId;
        const vReviewId = req.params.reviewId;

        console.log(vCourseId);
        console.log(vReviewId);

        if (!mongoose.Types.ObjectId.isValid(vCourseId) || !mongoose.Types.ObjectId.isValid(vReviewId)) {
            return res.status(400).json({
                errorMessage: `Bad request!`
            })
        } else {
            const vCourseGetedResult = await Course.findById(vCourseId).populate('reviews');
            console.log(vCourseGetedResult);
            const vReview = await vCourseGetedResult.reviews.find(review => review._id.toString() === vReviewId);
            return !vReview ? res.status(404).json({
                errorMessage: `Không tìm thấy review`
            }) : res.status(200).json({
                result: vReview
            });
        };
    } catch (err) {
        console.log(err);
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
    };
};

const updateAReviewOfCourseController = async (req, res) => {
    try {
        const vCourseId = req.params.courseId;
        const vReviewId = req.params.reviewId;
        const { stars, note } = req.body;
        const newReviewData = {
            stars: stars,
            note: note
        };

        if (!mongoose.Types.ObjectId.isValid(vCourseId) || !mongoose.Types.ObjectId.isValid(vReviewId)) {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        if (stars && typeof (stars) !== 'number') {
            return res.status(400).json({
                errorMessage: 'Bad request!'
            });
        };

        if (note && typeof (note) !== 'string') {
            return res.status(400).json({
                errorMessage: 'Bad request!'
            });
        };

        const vResult = await Review.findByIdAndUpdate(vReviewId, newReviewData);

        res.status(200).json({
            message: `update OK!`
        });

    } catch (err) {
        console.log(err);
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
    };
};

export {
    createAReviewController,
    getAllReviewOfCourseController,
    deleteAReviewOfCourse,
    getAReviewOfCourseController,
    updateAReviewOfCourseController
}