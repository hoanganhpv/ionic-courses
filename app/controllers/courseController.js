import mongoose from "mongoose";
import { Course } from "../models/courseModel.js";

const getAllCourseController = async (req, res) => {
    try {
        const allCourse = await Course.find();
        res.status(200).json({
            message: `Đã lấy ra tất cả khóa học!`,
            courses: allCourse
        });
    } catch (err) {
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
    };
};

const getACourseByIdController = async (req, res) => {
    try {
        const vId = req.params.paramId;
        const vCourseGeted = await Course.findById(vId);
        if (!mongoose.Types.ObjectId.isValid(vId)) {
            res.status(400).json({
                errorMessage: `Id không hợp lệ!`
            });
        };
        res.status(200).json({
            message: `Đã tìm được khóa học theo id truyền vào!`,
            course: vCourseGeted
        })
    } catch (err) {
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
    };
};

const createACourseController = async (req, res) => {
    try {
        const { courseCode, courseName, price, discountPrice, duration, level, coverImage, teacherName, teacherPhoto, isPopolar, isTrending } = req.body;
        const vNewCourse = new Course({
            _id: new mongoose.Types.ObjectId(),
            courseCode: courseCode,
            courseName: courseName,
            price: price,
            discountPrice: discountPrice,
            duration: duration,
            level: level,
            coverImage: coverImage,
            teacherName: teacherName,
            teacherPhoto: teacherPhoto,
            isPopolar: isPopolar,
            isTrending: isTrending
        });

        // validate
        if (!courseCode || typeof (courseCode) !== 'string') {
            return res.status(400).json({
                errorMessage: `courseCode không hợp lệ!`
            })
        };

        if (!courseName || typeof (courseName) !== 'string') {
            return res.status(400).json({
                errorMessage: `courseName không hợp lệ!`
            })
        };

        if (!price || typeof (price) !== 'number') {
            return res.status(400).json({
                errorMessage: `price không hợp lệ!`
            })
        };

        if (!discountPrice || typeof (discountPrice) !== 'number') {
            return res.status(400).json({
                errorMessage: `discountPrice không hợp lệ!`
            })
        };

        if (!duration || typeof (duration) !== 'string') {
            return res.status(400).json({
                errorMessage: `duration không hợp lệ!`
            })
        };

        if (!level || typeof (level) !== 'string') {
            return res.status(400).json({
                errorMessage: `level không hợp lệ!`
            })
        };

        if (!coverImage || typeof (coverImage) !== 'string') {
            return res.status(400).json({
                errorMessage: `coverImage không hợp lệ!`
            })
        };

        if (!teacherName || typeof (teacherName) !== 'string') {
            return res.status(400).json({
                errorMessage: `teacherName không hợp lệ!`
            })
        };

        if (!teacherPhoto || typeof (teacherPhoto) !== 'string') {
            return res.status(400).json({
                errorMessage: `teacherPhoto không hợp lệ!`
            })
        };

        if (isPopolar && typeof (isPopolar) !== 'boolean') {
            return res.status(400).json({
                errorMessage: `isPopolar không hợp lệ!`
            })
        };

        if (isTrending && typeof (isTrending) !== 'boolean') {
            return res.status(400).json({
                errorMessage: `isTrending không hợp lệ!`
            })
        };

        const resultCreate = await Course.create(vNewCourse);
        res.status(201).json({
            messgae: `Đã tạo thành công khóa học`,
            result: vNewCourse
        })
    } catch (err) {
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
    };
};

const updateCourseByIdController = async (req, res) => {
    try {
        const vUpdateId = req.params.paramId;
        const { courseCode, courseName, price, discountPrice, duration, level, coverImage, teacherName, teacherPhoto, isPopolar, isTrending } = req.body;
        const vUpdateData = {
            courseCode: courseCode,
            courseName: courseName,
            price: price,
            discountPrice: discountPrice,
            duration: duration,
            level: level,
            coverImage: coverImage,
            teacherName: teacherName,
            teacherPhoto: teacherPhoto,
            isPopolar: isPopolar,
            isTrending: isTrending
        };

        // validate 
        if (!mongoose.Types.ObjectId.isValid(vUpdateId)) {
            return res.status(400).json({
                errorMessage: `Id không hợp lệ!`
            })
        }

        if (courseCode && typeof (courseCode) !== 'string') {
            return res.status(400).json({
                errorMessage: `courseCode không hợp lệ!`
            })
        };

        if (courseName && typeof (courseName) !== 'string') {
            return res.status(400).json({
                errorMessage: `courseName không hợp lệ!`
            })
        };

        if (price && typeof (price) !== 'number') {
            return res.status(400).json({
                errorMessage: `price không hợp lệ!`
            })
        };

        if (discountPrice && typeof (discountPrice) !== 'number') {
            return res.status(400).json({
                errorMessage: `discountPrice không hợp lệ!`
            })
        };

        if (duration && typeof (duration) !== 'string') {
            return res.status(400).json({
                errorMessage: `duration không hợp lệ!`
            })
        };

        if (level && typeof (level) !== 'string') {
            return res.status(400).json({
                errorMessage: `level không hợp lệ!`
            })
        };

        if (coverImage && typeof (coverImage) !== 'string') {
            return res.status(400).json({
                errorMessage: `coverImage không hợp lệ!`
            })
        };

        if (teacherName && typeof (teacherName) !== 'string') {
            return res.status(400).json({
                errorMessage: `teacherName không hợp lệ!`
            })
        };

        if (teacherPhoto && typeof (teacherPhoto) !== 'string') {
            return res.status(400).json({
                errorMessage: `teacherPhoto không hợp lệ!`
            })
        };

        if (isPopolar && typeof (isPopolar) !== 'boolean') {
            return res.status(400).json({
                errorMessage: `isPopolar không hợp lệ!`
            })
        };

        if (isTrending && typeof (isTrending) !== 'boolean') {
            return res.status(400).json({
                errorMessage: `isTrending không hợp lệ!`
            })
        };

        if (!courseCode && !courseName && !price && !discountPrice && !duration && !level && !coverImage && !teacherName && !teacherPhoto && !isPopolar && !isTrending) {
            return res.status(404).json({
                errorMessage: `Không tìm thấy dữ liệu cần được update!`
            })
        }

        const vCourseUpdated = await Course.findByIdAndUpdate(vUpdateId, vUpdateData);
        res.status(201).json({
            message: `Đã cập nhật thành công khóa học!`,
            result: vCourseUpdated
        });
    } catch (err) {
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
    }
};

const deleteACourseById = async (req, res) => {
    try {
        const vDeleteCourseId = req.params.paramId;
        if (!mongoose.Types.ObjectId.isValid(vDeleteCourseId)) {
            res.status(400).json({
                errorMessgae: `Id Không hợp lệ!`
            });
        };

        const vDeleteResult = await Course.findOneAndDelete({ _id: vDeleteCourseId });
        if (!vDeleteResult) {
            res.status(404).json({
                errorMessage: `Id này đã không tồn tại trong cơ sở dữ liệu!`
            })
        } else {
            res.status(204).json({
                messgae: `Xóa dữ liệu thành công !`
            });
        };

    } catch (err) {
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
    };
};

export {
    getAllCourseController,
    getACourseByIdController,
    createACourseController,
    updateCourseByIdController,
    deleteACourseById
}