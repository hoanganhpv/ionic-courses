$(document).ready(() => {
  let gCourseIdToFix = null;
  let gCourseCols = [
    // Tạo ra 1 mảng chứa tên các cột của DataTable -> tên các cột này để DataTable tìm trong các key của Data load vào chứ ko phải tên cột hiện trên bảng, -> cho nên phải giống với các giá trị key trong Data đưa vào DataTable -> trường hợp 1 số cột như cột số thứ tự ko có trong Data đưa vào thì vẫn để đại 1 tên cho cột STT sau đó dùng render cho cột đó.
    "Stt", // dùng render ở columnDefs trong DataTable cho cột này
    "id",
    "courseCode",
    "courseName",
    "price",
    "discountPrice",
    "duration",
    "level",
    "coverImage",
    "teacherName",
    "teacherPhoto",
    "Action", // cái này ko có trong dữ liệu đưa vào. nhưng hàng nào cũng đều giống nhau. -> nên xài defaultContent xét cứng dữ liệu cột này ở mỗi hàng.
  ];
  let gOrders = 0; // Tạo 1 biến Số thứ tự = 0 trước để sử dụng cho hàm render số thứ tụ.
  onPageLoading();

  function onPageLoading() {
    createTable(); // Khi load trang thì sẽ thực hiện hàm tạo bảng
    callApiToGetFullCourse(); // sau khi tạo bảng sẽ gọi API để lấy danh sách khóa học về.
  }

  $("#inser-btn").on("click", () => {
    $("#insert-modal").modal("show");
  }); // Khi nhấn nút thêm khóa học thì sẽ thực hiện hàm hiện modal thêm khóa học

  $(document).on("click", "#save-new-course-btn", () => {
    let vNewCourseGeted = getNewCouresData(); // Hàm getNewCouresData() sẽ return về 1 object khóa học mới -> các giá trị của obj này được lấy giá trị từ các ô input mình nhập vào ở modal thêm mới khóa học. sau khi trả về Obj khóa học này -> gán obj này vào biến vNewCourseGeted
    console.log(vNewCourseGeted);
    callApiToCreateCourse(vNewCourseGeted); // chạy hàm callApiToCreateCourse (truyền khóa học mới lấy được ở modal vào hàm này) -> đây là hàm call API để tạo mới 1 khóa học.
  }); // Khi nhấn vào nút lưu ở trên modal tạo mới khóa học

  $(document).on("click", "#fix-course", (e) => {
    let vCurrDataClicked = $("#course-table")
      .DataTable()
      .row($(e.target).closest("tr"))
      .data(); // Lúc này biến vCurrDataClicked sẽ được gán bằng 1 obj chính là dữ liệu của khóa học ở dòng mình đang nhấn sửa
    $("#fix-modal").modal("show"); // sau khi nhấn sửa thì chạy hàm hiện lên modal sửa khóa học
    gCourseIdToFix = vCurrDataClicked["id"]; // gCourseIdToFix là biến toàn cục dùng để lưu id của khóa học hiện đang cần sửa -> để xíu nữa bỏ id này vào hàm call API sửa khóa học
    putCurrCourseFixingIntoFixModal(vCurrDataClicked); // Hàm này bỏ dữ liệu của khóa học mình vừa nhấn sửa vào các ô thông tin hiện trên modal
    console.log(vCurrDataClicked);
  }); // Khi nhấn nút sửa khóa học ở mỗi dòng

  $(document).on("click", "#delete-course", (e) => {
    $("#delete-modal").modal("show");
    let vCourseToDelete = $("#course-table")
      .DataTable()
      .row($(e.target).closest("tr"))
      .data();
    $("#save-delete-course-btn").on("click", () => {
      callApiToDelete(vCourseToDelete["id"]);
    });
  });

  $("#save-fix-course-btn").on("click", () => {
    let vCourseFixing = getFixCouresData(); // hàm getFixCouresData() return về 1 obj các thông tin của khóa học vừa được sửa (vì trên modal sửa này không có trường ID nên hàm này sẽ không thể trả về ID -> đó là lí do vì sao phải lưu id khóa học cần sửa vào 1 biến toàn cục trước ở trên)
    console.log(`đây là dữ liệu để sửa: ${JSON.stringify(vCourseFixing)}`);
    callApiToFixCourse(vCourseFixing); // thực hiện gọi hàm callApiToFixCourse() để call API UPDATE thông tin khóa học.
  }); // hàm này thực hiện khi -> sau khi hiện lên modal sửa khóa học, người dùng sửa thông tin xong -> nhấn nút lưu trên modal -> thực hiện hàm

  function handleCallApiSuccess(paramRe) {
    console.log(
      `%cLấy full course thành công\n${JSON.stringify(paramRe)}`,
      "color:green"
    );
    console.log(paramRe);
    loadDataIntoCourseTable(paramRe);
  } // hàm thực hiện khi gọi API để lấy danh sách khóa học thành công

  function handleCallApiError(paramAjaxContext) {
    console.log(
      `%cLấy full course thất bại\n${JSON.stringify(paramAjaxContext)}`,
      "color:red"
    );
  } // hàm thực hiện khi gọi API để lấy danh sách khóa học thất bại

  // Hàm Gọi API để lấy danh sách trả về tất cả khóa học
  function callApiToGetFullCourse() {
    $.ajax({
      type: "GET",
      contentType: "application/json",
      dataType: "json",
      url: "https://624abe0dfd7e30c51c110ac6.mockapi.io/api/v1/courses",
      async: true,
      success: (re) => {
        handleCallApiSuccess(re); // Khi gọi API thành công (success) -> chạy hàm này (handleCallApiSuccess)
      },
      error: (err) => {
        handleCallApiError(err); // Khi gọi API thất bại (error) -> chạy hàm này (handleCallApiError)
      },
    });
  }

  // Hàm tạo bảng danh sách các khóa học (sử dụng pluggin của jquery -> DataTable)
  function createTable() {
    $("#course-table").DataTable({
      columns: [
        {
          data: gCourseCols[0],
        },
        {
          data: gCourseCols[1],
        },
        {
          data: gCourseCols[2],
        },
        {
          data: gCourseCols[3],
        },
        {
          data: gCourseCols[4],
        },
        {
          data: gCourseCols[5],
        },
        {
          data: gCourseCols[6],
        },
        {
          data: gCourseCols[7],
        },
        {
          data: gCourseCols[8],
        },
        {
          data: gCourseCols[9],
        },
        {
          data: gCourseCols[10],
        },
        {
          data: gCourseCols[11],
        },
      ],
      columnDefs: [
        {
          targets: 0,
          className: "text-center align-middle",
          render: renderOrders, // render ra số thứ tự mỗi dòng
        },
        {
          targets: 1,
          className: "text-center align-middle",
        },
        {
          targets: 2,
          className: "text-center align-middle",
        },
        {
          targets: 3,
          className: "text-center align-middle",
        },
        {
          targets: 4,
          className: "text-center align-middle",
        },
        {
          targets: 5,
          className: "text-center align-middle",
        },
        {
          targets: 6,
          className: "text-center align-middle",
        },
        {
          targets: 7,
          className: "text-center align-middle",
        },
        {
          targets: 8,
          className: "text-center align-middle self",
        },
        {
          targets: 9,
          className: "text-center align-middle self",
        },
        {
          targets: 10,
          className: "text-center align-middle self",
        },
        {
          targets: 11,
          className: "text-center align-middle",
          defaultContent: `<div class="row">
                    <div class="col-xl-6">
                        <button id="fix-course" class="btn btn-warning text-center align-middle">Sửa</button>
                    </div>
                    <div class="col-xl-6">
                        <button id="delete-course" class="btn btn-danger text-center align-middle">Xóa</button>
                    </div>
                </div>`, // dùng defaultContent để set cứng HTML ở mỗi td cột này ở mỗi dòng
        },
      ],
    });
  }

  function renderOrders() {
    return ++gOrders;
  } // Hàm render về số thự tự -> mỗi lần được gọi sẽ tăng gOrders lên 1 đơn vị

  function loadDataIntoCourseTable(paramCourseData) {
    let vTable = $("#course-table").DataTable();
    vTable.clear();
    vTable.rows.add(paramCourseData);
    vTable.draw();
  } // hàm bỏ dữ liệu vào table để load ra dữ liệu

  function callApiToCreateCourse(paramNewCourse) {
    $.ajax({
      type: "POST",
      data: JSON.stringify(paramNewCourse),
      dataType: "json",
      contentType: "application/json",
      url: "https://624abe0dfd7e30c51c110ac6.mockapi.io/api/v1//courses",
      async: true,
      success: (re) => {
        console.log("đã tạo thành công");
        console.log(JSON.stringify(re));
        resetCourseTable(); // Sau khi tạo khóa học thành công thì chạy hàm này (resetCourseTable) -> dùng để xóa dữ liệu trên các ô input của modal thêm khóa học -> nếu không xóa thì khi thêm 1 khóa học thứ 2 -> modal hiện lên sẽ vẫn còn thông tin của khóa học trước. -> xóa dữ liệu xong hàm sẽ ẩn modal đi
      },
      error: (error) => {
        console.log("thất bại");
      },
    });
  } // Hàm gọi API để tạo mới 1 khóa học

  function callApiToFixCourse(paramNewCourse) {
    $.ajax({
      type: "PUT",
      data: JSON.stringify(paramNewCourse),
      dataType: "json",
      contentType: "application/json",
      url: `https://624abe0dfd7e30c51c110ac6.mockapi.io/api/v1/courses/${gCourseIdToFix}`, // truyền biến ID khóa học cần sửa ở toàn cục vào đây
      async: true,
      success: (re) => {
        console.log("đã Sửa thành công");
        console.log(JSON.stringify(re));
        $("#fix-modal").modal("hide"); // ẩn modal sửa khóa học đi
        callApiToGetFullCourse(); // sau khi sửa thành công chạy hàm callApiToGetFullCourse lần nữa để cập nhật lại bảng
      },
      error: (error) => {
        console.log("thất bại");
      },
    });
  } // hàm gọi API để sửa thông tin khóa học theo ID

  function getNewCouresData() {
    return {
      courseCode: $("#new-course-code").val(),
      courseName: $("#new-course-name").val(),
      price: $("#new-course-price").val(),
      discountPrice: $("#new-course-discount").val(),
      duration: $("#new-course-duration").val(),
      level: $("#new-course-level").val(),
      coverImage: $("#new-course-cover-img").val(),
      teacherName: $("#new-course-teacher").val(),
      teacherPhoto: $("#new-course-teacher-img").val(),
      isPopular: $("#new-course-popular").val(),
      isTrending: $("#new-course-trending").val(),
    };
  } // hàm return về obj chứa các thông tin của khóa học người dùng nhập vào modal tạo mới khóa học

  function getFixCouresData() {
    return {
      courseCode: $("#fix-course-code").val(),
      courseName: $("#fix-course-name").val(),
      price: $("#fix-course-price").val(),
      discountPrice: $("#fix-course-discount").val(),
      duration: $("#fix-course-duration").val(),
      level: $("#fix-course-level").val(),
      coverImage: $("#fix-course-cover-img").val(),
      teacherName: $("#fix-course-teacher").val(),
      teacherPhoto: $("#fix-course-teacher-img").val(),
      isPopular: $("#fix-course-popular").val(),
      isTrending: $("#fix-course-trending").val(),
    };
  } //hàm return về obj chứa các thông tin của khóa học người dùng sửa ở modal sửa khóa học

  function resetCourseTable() {
    $("#insert-modal").modal("hide"); // Ẩn đi modal thêm mới khóa học
    clearInsertModalContent(); // xóa đi các dữ liệu còn dính trên các ô input sau khi call API thành công
    callApiToGetFullCourse(); // chạy lại hàm callApiToGetFullCourse để cập nhật lại table
  } // hàm reset modal

  function clearInsertModalContent() {
    $("#new-course-code").val("");
    $("#new-course-name").val("");
    $("#new-course-price").val("");
    $("#new-course-discount").val("");
    $("#new-course-duration").val("");
    $("#new-course-level").val("");
    $("#new-course-cover-img").val("");
    $("#new-course-teacher").val("");
    $("#new-course-teacher-img").val("");
    $("#new-course-popular").val("");
    $("#new-course-trending").val("");
  } // Hàm xóa đi các dữ liệu còn dính trên các ô input sau khi call API thành công

  function putCurrCourseFixingIntoFixModal(paramCurrCourseClicked, paramId) {
    $("#fix-course-code").val(paramCurrCourseClicked["courseCode"]);
    $("#fix-course-name").val(paramCurrCourseClicked["courseName"]);
    $("#fix-course-price").val(paramCurrCourseClicked["price"]);
    $("#fix-course-discount").val(paramCurrCourseClicked["discountPrice"]);
    $("#fix-course-duration").val(paramCurrCourseClicked["duration"]);
    $("#fix-course-level").val(paramCurrCourseClicked["level"]);
    $("#fix-course-cover-img").val(paramCurrCourseClicked["coverImage"]);
    $("#fix-course-teacher").val(paramCurrCourseClicked["teacherName"]);
    $("#fix-course-teacher-img").val(paramCurrCourseClicked["teacherPhoto"]);

    if (paramCurrCourseClicked["isPopular"]) {
      $("#fix-course-popular").val("true");
    } else {
      $("#fix-course-popular").val("false");
    }

    if (paramCurrCourseClicked["isTrending"]) {
      $("#fix-course-trending").val("true");
    } else {
      $("#fix-course-trending").val("false");
    }
  } //  hàm bỏ các thông tin của khóa học hiện tại đang cần sửa vào modal sửa khóa học

  function callApiToDelete(paramId) {
    $.ajax({
      type: "DELETE",
      url: `https://624abe0dfd7e30c51c110ac6.mockapi.io/api/v1/courses/${paramId}`,
      async: true,
      success: (re) => {
        console.log(JSON.stringify(re));
        $("#delete-modal").modal("hide");
        callApiToGetFullCourse();
      },
      error: (err) => {
        alert("xóa thất bại");
        console.log(JSON.stringify(err));
        $("#delete-modal").modal("hide");
      },
    });
  }
}); // Hàm call API để xóa khóa học hiện tại
