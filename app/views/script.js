var gCoursesDB = {
  description: "This DB includes all courses in system",
  courses: [
    {
      id: 1,
      courseCode: "FE_WEB_ANGULAR_101",
      courseName: "How to easily create a website with Angular",
      price: 750,
      discountPrice: 600,
      duration: "3h 56m",
      level: "Beginner",
      coverImage: "images/courses/course-angular.jpg",
      teacherName: "Morris Mccoy",
      teacherPhoto: "images/teacher/morris_mccoy.jpg",
      isPopular: false,
      isTrending: true,
    },
    {
      id: 2,
      courseCode: "BE_WEB_PYTHON_301",
      courseName: "The Python Course: build web application",
      price: 1050,
      discountPrice: 900,
      duration: "4h 30m",
      level: "Advanced",
      coverImage: "images/courses/course-python.jpg",
      teacherName: "Claire Robertson",
      teacherPhoto: "images/teacher/claire_robertson.jpg",
      isPopular: false,
      isTrending: true,
    },
    {
      id: 5,
      courseCode: "FE_WEB_GRAPHQL_104",
      courseName: "GraphQL: introduction to graphQL for beginners",
      price: 850,
      discountPrice: 650,
      duration: "2h 15m",
      level: "Intermediate",
      coverImage: "images/courses/course-graphql.jpg",
      teacherName: "Ted Hawkins",
      teacherPhoto: "images/teacher/ted_hawkins.jpg",
      isPopular: true,
      isTrending: false,
    },
    {
      id: 6,
      courseCode: "FE_WEB_JS_210",
      courseName: "Getting Started with JavaScript",
      price: 550,
      discountPrice: 300,
      duration: "3h 34m",
      level: "Beginner",
      coverImage: "images/courses/course-javascript.jpg",
      teacherName: "Ted Hawkins",
      teacherPhoto: "images/teacher/ted_hawkins.jpg",
      isPopular: true,
      isTrending: true,
    },
    {
      id: 8,
      courseCode: "FE_WEB_CSS_111",
      courseName: "CSS: ultimate CSS course from beginner to advanced",
      price: 750,
      discountPrice: 600,
      duration: "3h 56m",
      level: "Beginner",
      coverImage: "images/courses/course-css.jpg",
      teacherName: "Juanita Bell",
      teacherPhoto: "images/teacher/juanita_bell.jpg",
      isPopular: true,
      isTrending: true,
    },
    {
      id: 9,
      courseCode: "FE_WEB_WORDPRESS_111",
      courseName: "Complete Wordpress themes & plugins",
      price: 1050,
      discountPrice: 900,
      duration: "4h 30m",
      level: "Intermediate",
      coverImage: "images/courses/course-wordpress.jpg",
      teacherName: "Clevaio Simon",
      teacherPhoto: "images/teacher/clevaio_simon.jpg",
      isPopular: true,
      isTrending: false,
    },
    {
      id: 10,
      courseCode: "FE_UIUX_COURSE_211",
      courseName: "Thinkful UX/UI Design Bootcamp",
      price: 950,
      discountPrice: 700,
      duration: "5h 30m",
      level: "Advanced",
      coverImage: "images/courses/course-uiux.jpg",
      teacherName: "Juanita Bell",
      teacherPhoto: "images/teacher/juanita_bell.jpg",
      isPopular: false,
      isTrending: false,
    },
    {
      id: 11,
      courseCode: "FE_WEB_REACRJS_210",
      courseName: "Front-End Web Development with ReactJs",
      price: 1100,
      discountPrice: 850,
      duration: "6h 20m",
      level: "Advanced",
      coverImage: "images/courses/course-reactjs.jpg",
      teacherName: "Ted Hawkins",
      teacherPhoto: "images/teacher/ted_hawkins.jpg",
      isPopular: true,
      isTrending: true,
    },
    {
      id: 12,
      courseCode: "FE_WEB_BOOTSTRAP_101",
      courseName: "Bootstrap 4 Crash Course | Website Build & Deploy",
      price: 750,
      discountPrice: 600,
      duration: "3h 15m",
      level: "Intermediate",
      coverImage: "images/courses/course-bootstrap.png",
      teacherName: "Juanita Bell",
      teacherPhoto: "images/teacher/juanita_bell.jpg",
      isPopular: true,
      isTrending: false,
    },
    {
      id: 14,
      courseCode: "FE_WEB_RUBYONRAILS_310",
      courseName: "The Complete Ruby on Rails Developer Course",
      price: 2050,
      discountPrice: 1450,
      duration: "8h 30m",
      level: "Advanced",
      coverImage: "images/courses/course-rubyonrails.png",
      teacherName: "Claire Robertson",
      teacherPhoto: "images/teacher/claire_robertson.jpg",
      isPopular: false,
      isTrending: true,
    },
  ],
};

$(document).ready(onPageLoading);

function onPageLoading() {
    callApiToGetFullCourse()
  loadRecommendedCourse();

}

function loadRecommendedCourse() {
  let vRecommendCourse = gCoursesDB.courses;
  for (let bIndex = 0; bIndex < 4; bIndex++) {
    $("#course-recommended").append(
      `   <div class="card">
                    <img class="card-img-top" src=${vRecommendCourse[bIndex].coverImage} alt="Card image cap">
                    <div class="card-body">
                        <p class="card-title text-primary"><b>${vRecommendCourse[bIndex].courseName}</b></p>
                        <p class="card-text"><i class="fa-regular fa-clock"></i> &nbsp; ${vRecommendCourse[bIndex].duration} &nbsp; ${vRecommendCourse[bIndex].level}</p>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-sm-10">
                                <img src=${vRecommendCourse[bIndex].teacherPhoto} class="rounded-circle" style="width: 20%">
                                <small class="ml-2">${vRecommendCourse[bIndex].teacherName}</small>
                            </div>
                            <div class="col-sm-2 text-right mt-1">
                                <i class="fa-regular fa-bookmark"></i>
                            </div>
                        </div>
                    </div>
                </div>
            `
    );
  }
}

function loadMostPopularCourse(paramCourses) {
  for (let bIndex = 0; bIndex < 4; bIndex++) {
    $("#course-trending").append(
      `   <div class="card">
                    <img class="card-img-top" src=${paramCourses[bIndex].coverImage} alt="Card image cap">
                    <div class="card-body">
                        <p class="card-title text-primary"><b>${paramCourses[bIndex].courseName}</b></p>
                        <p class="card-text"><i class="fa-regular fa-clock"></i> &nbsp; ${paramCourses[bIndex].duration} &nbsp; ${paramCourses[bIndex].level}</p>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-sm-10">
                                <img src=${paramCourses[bIndex].teacherPhoto} class="rounded-circle" style="width: 20%">
                                <small class="ml-2">${paramCourses[bIndex].teacherName}</small>
                            </div>
                            <div class="col-sm-2 text-right mt-1">
                                <i class="fa-regular fa-bookmark"></i>
                            </div>
                        </div>
                    </div>
                </div>
            `
    );
  }
}

function loadTopTrendingCourse(paramCourses) {
  for (let bIndex = 0; bIndex < 4; bIndex++) {
    $("#course-popular").append(
      `   <div class="card">
                    <img class="card-img-top" src=${paramCourses[bIndex].coverImage} alt="Card image cap">
                    <div class="card-body">
                        <p class="card-title text-primary"><b>${paramCourses[bIndex].courseName}</b></p>
                        <p class="card-text"><i class="fa-regular fa-clock"></i> &nbsp; ${paramCourses[bIndex].duration} &nbsp; ${paramCourses[bIndex].level}</p>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-sm-10">
                                <img src=${paramCourses[bIndex].teacherPhoto} class="rounded-circle" style="width: 20%">
                                <small class="ml-2">${paramCourses[bIndex].teacherName}</small>
                            </div>
                            <div class="col-sm-2 text-right mt-1">
                                <i class="fa-regular fa-bookmark"></i>
                            </div>
                        </div>
                    </div>
                </div>
            `
    );
  }
};

function handleCallApiSuccess(paramRe) {
    console.log(`%cLấy full course thành công\n${JSON.stringify(paramRe)}`, 'color:green');
    loadMostPopularCourse(paramRe);
    loadTopTrendingCourse(paramRe);
};

function handleCallApiError(paramAjaxContext) {
    console.log(`%cLấy full course thất bại\n${JSON.stringify(paramAjaxContext)}`, 'color:red');
};

function callApiToGetFullCourse() {
    $.ajax({
        type: 'GET',
        contentType: 'application/json',
        dataType: 'json',
        url: 'https://630890e4722029d9ddd245bc.mockapi.io/api/v1/courses',
        success: (re) => {
            handleCallApiSuccess(re);
        },
        error: (ajaxContext) => {
            handleCallApiError(ajaxContext);
        }
    });
};


