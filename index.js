import express from "express";
import path from "path";
import mime from "mime-types";
import { fileURLToPath } from "url";
import mongoose from "mongoose";
import { Course } from "./app/models/courseModel.js";
import { courseRouter } from "./app/routes/courseRouter.js";
import { reviewRouter } from "./app/routes/reviewRouter.js";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const app = express();
const port = 8000;

try {
    await mongoose.connect('mongodb://127.0.0.1:27017/Course-365');
    console.log(`Connect to database Course-365 successfully!`);
} catch (err) {
    console.log(`Connect to Course-365 sever failed!`);
};

app.use(express.json());

app.get('/', (req, res,) => {
    const vViewsFile = path.join(__dirname + '/app/views/index.html');
    const vHeader = mime.lookup(vViewsFile);
    res.setHeader('Content-Type', vHeader);
    res.status(200).sendFile(vViewsFile);
});

app.use(express.static(path.join(__dirname + '/app/views'), {
    setHeaders: (res, path, stat) => {
        res.set('Content-Type', mime.lookup(path));
    }
}));

app.use('/api', courseRouter);
app.use('/api', reviewRouter);

app.listen(port, () => {
    console.log(`Course-365 Server is listening on port: ${port}`);
});